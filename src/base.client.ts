import Axios, { AxiosInstance } from 'axios';

export abstract class BaseClient {
  protected axios: AxiosInstance;

  constructor(apiKey: string) {
    this.axios = Axios.create({
      baseURL: 'https://systemapi.de/api/v2/lexoffice?api_key=c1df7fc7fc2084560e2f9e490143311dbbc9f3bd494ab1c78282b81ad01c3438',
      headers: {
        Authorization: `Bearer ${apiKey}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });
  }
}
